//array_combine
<?php
$a = array('green', 'red', 'yellow');
$b = array('avocado', 'apple', 'banana');
$c = array_combine($a, $b);
print_r($c);
echo "<br>";
?>


//array_filter
<?php
function test_odd($var)
{
    return($var & 1);
}

$a1=array("a","b",2,3,4);
print_r(array_filter($a1,"test_odd"));
echo "<br>";
?>

//array_flip
<?php
$input = array("oranges", "apples", "pears");
$flipped = array_flip($input);
print_r($flipped);
echo "<br>";
?>

//array_key_exsits
<?php
$s = array('first' => 1, 'second' => 4);
if (array_key_exists('first', $s)) {
    echo "The 'first' element is in the array";
}
echo"<br>";
?>


//array_keys
<?php
$array = array(0 => 100, "color" => "red");
print_r(array_keys($array));

$array = array("blue", "red", "green", "blue", "blue");
print_r(array_keys($array, "blue"));
echo "<br>";
?>

//array_pad
<?php
$a = array('size'=>'large', 'number'=>20, 'color'=>'red');
//print_r($a);
print_r(array_pad($a, 10, 'foo'));
echo "<br>";
?>


//array merge
<?php
$array1 = array("color" => "red", 2, 4);
$array2 = array("a", "b", "color" => "green", "shape" => "trapezoid", 4);
$result = array_merge($array1, $array2);
print_r($result);
echo "<br>";
?>


//array_pop
<?php
$stack = array("orange", "banana", "apple", "raspberry");
$fruit = array_pop($stack);
print_r($stack);
echo "<br>";
?>


//array_push
<?php
$stack = array("orange", "banana");
array_push($stack, "apple", "raspberry");
print_r($stack);
echo "<br>";
?>


//array_rand
<?php
$input = array("Neo", "Morpheus", "Trinity", "Cypher", "Tank");
$rand_keys = array_rand($input, 2);
//print_r($rand_keys);
echo $input[$rand_keys[0]] . "\n";
echo $input[$rand_keys[1]] . "\n";
echo "<br>";
?>


//array_replace
<?php
$base = array("orange", "banana", "apple", "raspberry");
$replacements = array(0 => "pineapple", 4 => "cherry");
$basket = array_replace($base, $replacements);
print_r($basket);
echo "<br>";
?>


//array_replace_recursive
<?php
$base = array('citrus' => array( "orange") , 'berries' => array("blackberry", "raspberry"), );
$replacements = array('citrus' => array('pineapple'), 'berries' => array('blueberry'));

$basket = array_replace_recursive($base, $replacements);
print_r($basket);
echo "<br>";
?>


//array_reverse
<?php
$input  = array("php", 4.0, array("green", "red"));
$reversed = array_reverse($input);
$preserved = array_reverse($input, true);

print_r($input);
echo "<br>";
print_r($reversed);
echo "<br>";
print_r($preserved);
echo "<br>";
?>

//array_shift
<?php
$stack = array("orange", "banana", "apple", "raspberry");
$fruit = array_shift($stack);
print_r($stack);
echo "<br>";
?>


//array_sum
<?php
$a = array(2, 4, 6, 8);
echo "sum(a) = " . array_sum($a) . "\n";

$b = array("a" => 1.2, "b" => 2.3, "c" => 3.4);
echo "sum(b) = " . array_sum($b) . "\n";
echo "<br>";
?>



//array_unique
<?php
$input = array("a" => "green", "red", "b" => "green", "blue", "red");
$result = array_unique($input);
print_r($result);echo "<br>";
?>


//array_unshift
<?php
$queue = array("orange", "banana");
array_unshift($queue, "apple", "raspberry");
print_r($queue);echo "<br>";
?>


//array_value
<?php
$array = array("size" => "XL", "color" => "gold");
print_r(array_values($array));echo "<br>";
?>


//array
<?php
$firstquarter = array(1 => 'January', 'February', 'March');
print_r($firstquarter);
?>


<?php
$fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");
asort($fruits);
foreach ($fruits as $key => $val) {
    echo "$key = $val\n";
}
echo "<br>";
?>


//compact
<?php
$city  = "San Francisco";
$state = "CA";
$event = "SIGGRAPH";

$location_vars = array("city", "state");

$result = compact("event", "nothing_here", $location_vars);
print_r($result);
echo "<br>";
?>


//current
<?php
$transport = array('foot', 'bike', 'car', 'plane');
$mode = current($transport);
print_r($mode);
echo "<b>";
?>


//count
<?php
$a[0] = 1;
$a[1] = 3;
$a[2] = 5;
$result = count($a);
print_r($result);
echo "<b>";
echo "<b>";
?>


//each
<?php
$foo = array("bob", "fred", "jussi", "jouni", "egon", "marliese");
$bar = each($foo);
print_r($bar);
?>

//end
<?php

$fruits = array('apple', 'banana', 'cranberry');
echo end($fruits); // cranberry

?>
